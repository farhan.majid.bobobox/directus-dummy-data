import { GraphQLClient, gql } from 'graphql-request'

/**
 * Get fields of a collection with the specified [collectionName].
 * 
 * @param {String} endpointSystem 
 * @param {String} accessToken 
 * @param {String} collectionName 
 * @returns array of fields_in_collection
 */
export async function getCollectionFields(endpointSystem, accessToken, collectionName) {
    const graphQLClientSystem = new GraphQLClient(endpointSystem, {
        headers: {
            authorization: `Bearer ${accessToken}`,
        },
    })
    const query = gql`
        {
            fields_in_collection(collection: "${collectionName}") {
                field
                type
            }
        }
    `;
    const data = await graphQLClientSystem.request(query);
    return data.fields_in_collection;
}