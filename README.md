# directus-dummy-data

## About

Create dummy data in Directus.

## Installation

1. Clone this repository.
2. Install packages by running:
   ```
   npm install
   ```
3. Run Directus server in your local machine.
4. Customize the `INPUT VARIABLES` in the `index.js` file according to your local Directus configuration.
5. Run the script by running:
   ```
   node index.js
   ```

## Assumptions

This script assumes that the NPS Feedback collection has these structures:
```json
[
   {
         "field": "branch_id",
         "type": "bigInteger"
   },
   {
         "field": "checkin_date",
         "type": "dateTime"
   },
   {
         "field": "checkout_date",
         "type": "dateTime"
   },
   {
         "field": "date_created",
         "type": "timestamp"
   },
   {
         "field": "date_updated",
         "type": "timestamp"
   },
   {
         "field": "email",
         "type": "string"
   },
   {
         "field": "entry_point",
         "type": "string"
   },
   {
         "field": "hotel_name",
         "type": "string"
   },
   {
         "field": "id",
         "type": "integer"
   },
   {
         "field": "list_feedback",
         "type": "json"
   },
   {
         "field": "nps_review",
         "type": "text"
   },
   {
         "field": "nps_score",
         "type": "integer"
   },
   {
         "field": "phone",
         "type": "string"
   },
   {
         "field": "pod_type",
         "type": "string"
   },
   {
         "field": "product_name",
         "type": "string"
   },
   {
         "field": "source",
         "type": "string"
   },
   {
         "field": "stay_id",
         "type": "bigInteger"
   },
   {
         "field": "user_created",
         "type": "string"
   },
   {
         "field": "user_name",
         "type": "string"
   },
   {
         "field": "user_updated",
         "type": "string"
   }
]
```

## Results

Dashboard created by the generated dummy data:

<img src="./images/bbx_directus_dashboard_1.png" width="600" />

<img src="./images/bbx_directus_dashboard_2.png" width="600" />

<img src="./images/bbx_directus_dashboard_3.png" width="600" />
