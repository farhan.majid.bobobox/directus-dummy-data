import { faker } from '@faker-js/faker';
import { GraphQLClient, gql } from 'graphql-request'
import {
    CONST_SOURCES,
    CONST_HOTEL_IDS,
    CONST_HOTEL_NAMES,
    CONST_PRODUCT_NAME_MAP,
    CONST_ENTRY_POINTS,
    CONST_POD_TYPES,
    CONST_FEEDBACK_TEMPLATES,
} from './constants.js';

/**
 * Create a new collection item.
 * 
 * @param {String} endpoint
 * @param {String} accessToken
 * @param {String} collectionName 
 * @param {Array} collectionFields 
 */
export async function createCollectionItem(endpoint, accessToken, collectionName, collectionFields) {
    const graphQLClient = new GraphQLClient(endpoint, {
        headers: {
            authorization: `Bearer ${accessToken}`,
        },
    })
    var hotelIndex = faker.datatype.number({
        min: 0,
        max: CONST_HOTEL_IDS.length - 1,
    });
    var firstName = faker.name.firstName();
    var lastName = faker.name.lastName();
    var checkInDate = faker.date.between('2022-01-01T00:00:00.000Z', new Date());
    var variables = {};
    collectionFields.forEach(field => {
        var value = '';
        if (field.field == 'branch_id') {
            value = CONST_HOTEL_IDS[hotelIndex];
            variables[field.field] = `${value}`;
        } else if (field.field == 'hotel_name') {
            value = CONST_HOTEL_NAMES[hotelIndex];
            variables[field.field] = value;
        } else if (field.field == 'email') {
            value = faker.internet.email(firstName, lastName);
            variables[field.field] = value;
        } else if (field.field == 'user_name') {
            value = `${firstName} ${lastName}`;
            variables[field.field] = value;
        } else if (field.field == 'phone') {
            value = faker.phone.number();
            variables[field.field] = value;
        } else if (field.field == 'product_name') {
            value = CONST_PRODUCT_NAME_MAP[CONST_HOTEL_IDS[hotelIndex]];
            variables[field.field] = value;
        } else if (field.field == 'source') {
            var index = faker.datatype.number({
                min: 0,
                max: CONST_SOURCES.length - 1,
            });
            value = CONST_SOURCES[index];
            variables[field.field] = value;
        } else if (field.field == 'entry_point') {
            var index = faker.datatype.number({
                min: 0,
                max: CONST_ENTRY_POINTS.length - 1,
            });
            value = CONST_ENTRY_POINTS[index];
            variables[field.field] = value;
        } else if (field.field == 'pod_type') {
            var index = faker.datatype.number({
                min: 0,
                max: CONST_POD_TYPES.length - 1,
            });
            value = CONST_POD_TYPES[index];
            variables[field.field] = value;
        } else if (field.field == 'nps_score') {
            value = faker.datatype.number({
                min: 5,
                max: 10,
            });
            variables[field.field] = value;
        } else if (field.field == 'checkin_date') {
            value = checkInDate.toISOString().substring(0, 10);
            variables[field.field] = value;
        } else if (field.field == 'nps_review') {
            value = faker.lorem.paragraph();
            variables[field.field] = value;
        } else if (field.field == 'stay_id') {
            value = faker.random.numeric(10);
            variables[field.field] = value;
        } else if (field.field == 'list_feedback') {
            var count = faker.datatype.number({
                min: 1,
                max: 3,
            });
            value = faker.helpers.arrayElements(CONST_FEEDBACK_TEMPLATES, count);
            variables[field.field] = value;
        } else if (field.field == 'checkout_date') {
            var checkoutDate = new Date(checkInDate);
            checkoutDate.setDate(checkoutDate.getDate() + 1);
            value = checkoutDate.toISOString().substring(0, 10);
            variables[field.field] = value;
        }
    });

    const mutation = gql`
        mutation CreateItem(
            $stay_id: String,
            $branch_id: String,
            $entry_point: String,
            $product_name: String,
            $hotel_name: String,
            $checkin_date: Date,
            $checkout_date: Date,
            $pod_type: String,
            $nps_score: Int,
            $nps_review: String,
            $list_feedback: JSON,
            $source: String,
            $email: String,
            $phone: String,
            $user_name: String
        ) {
            create_${collectionName}_item(
                data: { 
                    stay_id: $stay_id,
                    branch_id: $branch_id,
                    entry_point: $entry_point,
                    product_name: $product_name,
                    hotel_name: $hotel_name,
                    checkin_date: $checkin_date,
                    checkout_date: $checkout_date,
                    pod_type: $pod_type,
                    nps_score: $nps_score,
                    nps_review: $nps_review,
                    list_feedback: $list_feedback,
                    source: $source,
                    email: $email,
                    phone: $phone,
                    user_name: $user_name
                }
            ) {
                id
                stay_id
                branch_id
                entry_point
                product_name
                hotel_name
                checkin_date
                checkout_date
                pod_type
                nps_score
                nps_review
                list_feedback
                source
                email
                phone
                user_name
            }
        }
      `;

    await graphQLClient.request(mutation, variables);
    console.log(`Created item: ${variables.user_name}`);
}