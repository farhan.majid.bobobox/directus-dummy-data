/**
 * THIS FILE CONTAINS ALL CONSTANTS.
 */

/**
 * Possible values for "Source".
 */
export const CONST_SOURCES = [
    'Android',
    'iOS',
];

/**
 * Possible values for "Hotel ID".
 */
export const CONST_HOTEL_IDS = [
    1000,
    1002,
    1003,
    1004,
    1006,
    1007,
    1008,
    1009,
    1010,
    1011,
    1012,
    1013,
    1014,
    1017,
    1018,
    1019,
    1020,
    1021,
    1022,
    1023,
    1024,
    1025,
];

/**
 * Possible values for "Hotel Name".
 */
export const CONST_HOTEL_NAMES = [
    'Bobobox Paskal',
    'Bobobox Cipaganti',
    'Bobobox Dago',
    'Bobobox Pancoran',
    'Bobobox Kebayoran Baru',
    'Bobobox Alun Alun',
    'Bobobox Kota Tua',
    'Bobobox Kota Lama',
    'Bobobox Airport Hub Jakarta',
    'Bobobox Tanah Abang',
    'Bobobox Juanda Jakarta',
    'Bobobox Malioboro',
    'Bobobox Slamet Riyadi',
    'Bobocabin Ranca Upas',
    'Bobobox Mega Bekasi',
    'Bobocabin Cikole',
    'Bobocabin Signature Toba',
    'Bobobox Alun-alun Malang',
    'Bobobox ITC Kuningan',
    'Bobocabin Kintamani',
    'Bobocabin Gunung Mas',
    'Bobocabin Batu Malang',
];

/**
 * Mapping for "Product Name".
 */
export const CONST_PRODUCT_NAME_MAP = {
    1000: 'Bobohotel',
    1002: 'Bobohotel',
    1003: 'Bobohotel',
    1004: 'Bobohotel',
    1006: 'Bobohotel',
    1007: 'Bobohotel',
    1008: 'Bobohotel',
    1009: 'Bobohotel',
    1010: 'Bobohotel',
    1011: 'Bobohotel',
    1012: 'Bobohotel',
    1013: 'Bobohotel',
    1014: 'Bobohotel',
    1017: 'Bobocabin',
    1018: 'Bobohotel',
    1019: 'Bobocabin',
    1020: 'Bobocabin',
    1021: 'Bobohotel',
    1022: 'Bobohotel',
    1023: 'Bobocabin',
    1024: 'Bobocabin',
    1025: 'Bobocabin',
};

/**
 * Possible values for "Entry Point"
 */
export const CONST_ENTRY_POINTS = [
    'Check out',
    'History Stay',
    'Home',
];

/**
 * Possible values for "Pod Type"
 */
export const CONST_POD_TYPES = [
    'SKY',
    'EARTH',
    'SKY SINGLE',
    'EARTH SINGLE',
];

/**
 * Possible values for "List Feedback"
 */
export const CONST_FEEDBACK_TEMPLATES = [
    'Room hygiene',
    'Sleep comfort',
    'Host service',
    'Toilet',
    'Parking area',
    'Price accuracy',
    'Others',
];