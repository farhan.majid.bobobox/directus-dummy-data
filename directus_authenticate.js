import { GraphQLClient, gql } from 'graphql-request'

/**
 * Authenticate user with the specified [email] and [password].
 * 
 * @param {String} endpointSystem
 * @param {String} email
 * @param {String} password
 * @returns access token
 */
export async function authenticate(endpointSystem, email, password) {
    const graphQLClientSystem = new GraphQLClient(endpointSystem);

    const mutation = gql`
        mutation ($email: String!, $password: String!) {
            auth_login(email: $email, password: $password) {
                access_token
                refresh_token
            }   
        }
    `;
    const variables = {
        email,
        password,
    };
    const data = await graphQLClientSystem.request(mutation, variables);
    return data.auth_login.access_token;
}