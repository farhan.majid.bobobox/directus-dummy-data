import { authenticate } from './directus_authenticate.js';
// import { createCollectionItem } from './directus_create_collection_item.js';
import { createCollectionItemApi } from './directus_create_collection_item_api.js';
import { getCollectionFields } from './directus_get_collection_fields.js';

/**
 * INPUT VARIABLES.
 * 
 * Change these variables according to your need.
 */
const collectionNameNpsFeedbacks = 'nps_feedbacks';     // Collection name of NPS Feedback
const endpoint = 'http://0.0.0.0:8055/graphql'          // Directus GraphQL endpoint
const endpointApi = 'http://0.0.0.0:8055'          // Directus endpoint
const email = 'admin@example.com';                      // Directus user email address
const password = 'password';                            // Directus user password
const numItem = 250;                                    // Number of items that want to be generated in the NPS Feedback collection

/**
 * Derived variables.
 */
const endpointSystem = `${endpoint}/system`

/**
 * Step 1: Authenticate user. Save the access token for the next steps.
 */
const accessToken = await authenticate(endpointSystem, email, password);
// console.log(accessToken);

/**
 * Step 2: Get NPS Feedback collection's fields.
 */
const collectionFields = await getCollectionFields(endpointSystem, accessToken, collectionNameNpsFeedbacks);
// console.log(collectionFields);

/**
 * Step 3: Create new N items of NPS Feedback collections.
 */
for (var i = 0; i < numItem; i++) {
    // await createCollectionItem(endpoint, accessToken, collectionNameNpsFeedbacks, collectionFields);
    await createCollectionItemApi(endpointApi, accessToken, collectionNameNpsFeedbacks, collectionFields);
}