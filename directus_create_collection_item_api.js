import { faker } from '@faker-js/faker';
import axios from 'axios';
import {
    CONST_SOURCES,
    CONST_HOTEL_IDS,
    CONST_HOTEL_NAMES,
    CONST_PRODUCT_NAME_MAP,
    CONST_ENTRY_POINTS,
    CONST_POD_TYPES,
    CONST_FEEDBACK_TEMPLATES,
} from './constants.js';

/**
 * Create a new collection item.
 * 
 * @param {String} endpoint
 * @param {String} accessToken
 * @param {String} collectionName 
 * @param {Array} collectionFields 
 */
export async function createCollectionItemApi(endpoint, accessToken, collectionName, collectionFields) {
    var hotelIndex = faker.datatype.number({
        min: 0,
        max: CONST_HOTEL_IDS.length - 1,
    });
    var firstName = faker.name.firstName();
    var lastName = faker.name.lastName();
    var checkInDate = faker.date.between('2022-01-01T00:00:00.000Z', new Date());
    var variables = {};
    collectionFields.forEach(field => {
        var value = '';
        if (field.field == 'branch_id') {
            value = CONST_HOTEL_IDS[hotelIndex];
            variables[field.field] = `${value}`;
        } else if (field.field == 'hotel_name') {
            value = CONST_HOTEL_NAMES[hotelIndex];
            variables[field.field] = value;
        } else if (field.field == 'email') {
            value = faker.internet.email(firstName, lastName);
            variables[field.field] = value;
        } else if (field.field == 'user_name') {
            value = `${firstName} ${lastName}`;
            variables[field.field] = value;
        } else if (field.field == 'phone') {
            value = faker.phone.number();
            variables[field.field] = value;
        } else if (field.field == 'product_name') {
            value = CONST_PRODUCT_NAME_MAP[CONST_HOTEL_IDS[hotelIndex]];
            variables[field.field] = value;
        } else if (field.field == 'source') {
            var index = faker.datatype.number({
                min: 0,
                max: CONST_SOURCES.length - 1,
            });
            value = CONST_SOURCES[index];
            variables[field.field] = value;
        } else if (field.field == 'entry_point') {
            var index = faker.datatype.number({
                min: 0,
                max: CONST_ENTRY_POINTS.length - 1,
            });
            value = CONST_ENTRY_POINTS[index];
            variables[field.field] = value;
        } else if (field.field == 'pod_type') {
            var index = faker.datatype.number({
                min: 0,
                max: CONST_POD_TYPES.length - 1,
            });
            value = CONST_POD_TYPES[index];
            variables[field.field] = value;
        } else if (field.field == 'nps_score') {
            value = faker.datatype.number({
                min: 5,
                max: 10,
            });
            variables[field.field] = value;
        } else if (field.field == 'checkin_date') {
            value = checkInDate.toISOString().substring(0, 10);
            variables[field.field] = value;
        } else if (field.field == 'nps_review') {
            value = faker.lorem.paragraph();
            variables[field.field] = value;
        } else if (field.field == 'stay_id') {
            value = faker.random.numeric(10);
            variables[field.field] = value;
        } else if (field.field == 'list_feedback') {
            var count = faker.datatype.number({
                min: 1,
                max: 3,
            });
            value = faker.helpers.arrayElements(CONST_FEEDBACK_TEMPLATES, count);
            variables[field.field] = value;
        } else if (field.field == 'checkout_date') {
            var checkoutDate = new Date(checkInDate);
            checkoutDate.setDate(checkoutDate.getDate() + 1);
            value = checkoutDate.toISOString().substring(0, 10);
            variables[field.field] = value;
        }
    });

    await axios.post(`${endpoint}/items/${collectionName}`, variables, {
        headers: {
            authorization: `Bearer ${accessToken}`,
        },
    });
    console.log(`Created item: ${variables.user_name}`);
}